package org.cogeco.bitbucket.builder;

import java.util.List;

public class BuilderException extends Exception {
  public BuilderException(String message) {
    super(message);
  }

  public static BuilderException requiredValuesMissing(List<String> missingValues) {
    return new BuilderException(
      "Required values missing for valid build. " +
        "Missing values are: " +
        String.join(", ", missingValues) +
        "."
    );
  }
}
