package org.cogeco.bitbucket.builder;

import org.cogeco.bitbucket.models.Branch;
import org.cogeco.bitbucket.models.Destination;
import org.cogeco.bitbucket.models.Source;
import org.cogeco.bitbucket.models.requests.openPullRequest.OpenPullRequestBody;

import java.util.ArrayList;
import java.util.List;

public class OpenPullRequestBodyBuilder {
  private String sourceBranch;
  private String destinationBranch;
  private String title;
  private Boolean closeSourceBranch = true;
  private String description;

  public OpenPullRequestBody build() throws BuilderException {
    OpenPullRequestBody body = new OpenPullRequestBody();

    if (!this.isValidToBuild()) {
      throw BuilderException.requiredValuesMissing(this.buildListOfMissingValues());
    }

    body.source = this.buildSourceBranch();
    body.destination = this.buildDestinationBranch();
    body.closeSourceBranch = this.closeSourceBranch;
    body.title = this.title;
    body.description = this.description;

    return body;
  }

  public OpenPullRequestBodyBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  public OpenPullRequestBodyBuilder setSourceBranch(String sourceBranch) {
    this.sourceBranch = sourceBranch;
    return this;
  }

  public OpenPullRequestBodyBuilder setDestinationBranch(String destinationBranch) {
    this.destinationBranch = destinationBranch;
    return this;
  }

  public OpenPullRequestBodyBuilder setCloseSourceBranch(Boolean closeSourceBranch) {
    this.closeSourceBranch = closeSourceBranch;
    return this;
  }

  public OpenPullRequestBodyBuilder setDescription(String description) {
    this.description = description;
    return this;
  }

  private Source buildSourceBranch() {
    Branch sourceBranch = new Branch();
    sourceBranch.name = this.sourceBranch;
    Source source = new Source();
    source.branch = sourceBranch;

    return source;
  }

  private Destination buildDestinationBranch() {
    Branch destinationBranch = new Branch();
    destinationBranch.name = this.destinationBranch;
    Destination destination = new Destination();
    destination.branch = destinationBranch;

    return destination;
  }

  private Boolean isValidToBuild() {
    return this.title != null && this.sourceBranch != null && this.destinationBranch != null;
  }

  private List<String> buildListOfMissingValues() {
    List<String> missingValues = new ArrayList<>();

    if (this.title == null || this.title.isEmpty()) {
      missingValues.add("title");
    }

    if (this.sourceBranch == null || this.sourceBranch.isEmpty()) {
      missingValues.add("sourceBranch");
    }

    if (this.destinationBranch == null || this.destinationBranch.isEmpty()) {
      missingValues.add("destinationBranch");
    }

    return missingValues;
  }
}
