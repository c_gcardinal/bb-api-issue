package org.cogeco.bitbucket.client;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

class BasicAuthInterceptor implements Interceptor {
  private final String credentials;

  public BasicAuthInterceptor(String username, String password) {
    this.credentials = Credentials.basic(username, password);
  }

  @Override
  public @NotNull Response intercept(Chain chain) throws IOException {
    return chain.proceed(
      chain.request()
        .newBuilder()
        .header("Authorization", credentials)
        .build()
    );
  }
}
