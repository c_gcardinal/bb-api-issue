package org.cogeco.bitbucket.client;

import okhttp3.OkHttpClient;
import org.cogeco.bitbucket.models.Branches;
import org.cogeco.bitbucket.models.PullRequest;
import org.cogeco.bitbucket.models.requests.openPullRequest.OpenPullRequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.logging.Logger;

public class BitbucketClient {

  private static final Logger LOGGER = Logger.getLogger(BitbucketClient.class.getName());
  private final BitbucketService bitbucketService;

  public BitbucketClient(String username, String password) {
    this.bitbucketService = this.createBitbucketService(username, password);
  }

  public BitbucketClient(String username, String password, String uri) {
    this.bitbucketService = this.createBitbucketService(username, password, uri);
  }

  public Response<Branches> listBranches(String workspace, String repository) throws IOException, BitbucketClientException {
    return this.executeRequest(this.bitbucketService.listBranches(workspace, repository));
  }

  public Response<Response<Void>> deleteBranch(String workspace, String repository, String branch) throws IOException, BitbucketClientException {
    return this.executeRequest(this.bitbucketService.deleteBranch(workspace, repository, branch));
  }

  public Response<PullRequest> openPullRequest(String workspace, String repository, OpenPullRequestBody openPullRequestBody) throws IOException, BitbucketClientException {
    return this.executeRequest(this.bitbucketService.openPullRequest(workspace, repository, openPullRequestBody));
  }

  private <T> Response<T> executeRequest(Call<T> call) throws IOException, BitbucketClientException {
    LOGGER.info("Bitbucket REST API Request: " + call.request().url().toString());
    Response<T> response = call.execute();
    LOGGER.info("Bitbucket REST API Response: " + response.toString());

    if (!response.isSuccessful()) {
      throw new BitbucketClientException(
        "Failed to communicate with Bitbucket API." +
          "Code: " + response.code() + "." +
          "Message: " + response.code() + "." +
          "URL: " + call.request().url().toString() + "."
      );
    }

    return response;
  }

  private BitbucketService createBitbucketService(String username, String password) {
    return this.createBitbucketService(username, password, "https://api.bitbucket.org/2.0/");
  }

  private BitbucketService createBitbucketService(String username, String password, String uri) {
    OkHttpClient okHttpClient = new OkHttpClient.Builder()
      .addInterceptor(new BasicAuthInterceptor(username, password))
      .build();

    Retrofit retrofit = new Retrofit.Builder()
      .baseUrl(uri)
      .client(okHttpClient)
      .addConverterFactory(JacksonConverterFactory.create())
      .build();

    return retrofit.create(BitbucketService.class);
  }

}
