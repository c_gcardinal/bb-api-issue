package org.cogeco.bitbucket.client;

public class BitbucketClientException extends Exception {

  public BitbucketClientException(String message) {
    super(message);
  }

}
