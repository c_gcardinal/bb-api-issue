package org.cogeco.bitbucket.client;

import org.cogeco.bitbucket.models.requests.openPullRequest.OpenPullRequestBody;
import org.cogeco.bitbucket.models.Branches;
import org.cogeco.bitbucket.models.PullRequest;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.*;

public interface BitbucketService {
  @GET("repositories/{workspace}/{slug}/refs/branches")
  Call<Branches> listBranches(
    @Path("workspace") String workspace,
    @Path("slug") String slug
  );

  @DELETE("repositories/{workspace}/{slug}/refs/branches/{name}")
  Call<Response<Void>> deleteBranch(
    @Path("workspace") String workspace,
    @Path("slug") String slug,
    @Path("name") String name
  );

  @POST("repositories/{workspace}/{slug}/pullrequests")
  Call<PullRequest> openPullRequest(
    @Path("workspace") String workspace,
    @Path("slug") String slug,
    @Body OpenPullRequestBody openPullRequestBody
  );
}
