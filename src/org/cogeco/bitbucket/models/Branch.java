package org.cogeco.bitbucket.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Branch {
  public String name;

  public Links links;
}
