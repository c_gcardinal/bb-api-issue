package org.cogeco.bitbucket.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.annotation.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {

  public Link self;

  public Link html;

  @Nullable
  public Link commits;
}
