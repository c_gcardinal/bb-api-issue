package org.cogeco.bitbucket.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PullRequest {
  public String description;
  public String title;

  @JsonProperty("close_source_branch")
  public Boolean closeSourceBranch;

  public Links links;

}
