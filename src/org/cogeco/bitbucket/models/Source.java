package org.cogeco.bitbucket.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Source implements Serializable {

  public Branch branch;

}
