package org.cogeco.bitbucket.models.requests.openPullRequest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.cogeco.bitbucket.models.Destination;
import org.cogeco.bitbucket.models.Source;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OpenPullRequestBody implements Serializable {
  public String title;
  public Source source;
  public Destination destination;
  public String description;

  @JsonProperty("close_source_branch")
  public Boolean closeSourceBranch;
}
