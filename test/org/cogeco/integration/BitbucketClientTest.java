package org.cogeco.integration;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.cogeco.bitbucket.client.BitbucketClient;
import org.cogeco.bitbucket.client.BitbucketClientException;
import org.cogeco.bitbucket.models.requests.openPullRequest.OpenPullRequestBody;
import org.cogeco.bitbucket.models.Branches;
import org.cogeco.bitbucket.models.PullRequest;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.jupiter.api.Assertions.*;

public class BitbucketClientTest {

  @Rule
  WireMockRule wm = new WireMockRule(
    options()
      .usingFilesUnderDirectory("test/resources/bitbucket/responses")
  );

  private BitbucketClient client;

  @BeforeEach
  public void beforeEach() {
    wm.start();
    this.client = new BitbucketClient("a_username", "a_password", wm.baseUrl());
  }

  @AfterEach
  public void afterEach() {
    wm.stop();
  }

  @Test
  public void itCanListAllBranchesInARepository() throws IOException, BitbucketClientException {
    wm.stubFor(
      get(urlPathMatching("/repositories/cogeco-it/drupal/refs/branches"))
        .willReturn(aResponse().withBodyFile("get-branch.json"))
    );

    Branches branches = client.listBranches("cogeco-it", "drupal").body();

    assertNotNull(branches);
    // Basic validation of the response to make sure we have properly transformed the JSON
    assertEquals(5, branches.branches.size());
    assertEquals("dev", branches.branches.get(0).name);
  }

  @Test
  public void itCanOpenPullRequests() throws IOException, BitbucketClientException {
    wm.stubFor(
      post(urlPathMatching("/repositories/cogeco-it/deploy_firestorerest/pullrequests"))
        .willReturn(aResponse().withBodyFile("post-pull-request.json"))
    );

    PullRequest pullRequest = client.openPullRequest(
      "cogeco-it",
      "deploy_firestorerest",
      new OpenPullRequestBody()
      ).body();

    assertNotNull(pullRequest);
    assertEquals("Test PR", pullRequest.title);
    assertFalse(pullRequest.closeSourceBranch);
    assertEquals("https://bitbucket.org/cogeco-it/deploy_firestorerest/pull-requests/2", pullRequest.links.html.href);
  }

  @Test
  public void itCanDeleteBranches() throws IOException, BitbucketClientException {
    String deleteBranchRegEx = "/repositories/cogeco-it/drupal/refs/branches/a_branch";
    wm.stubFor(delete(urlPathMatching(deleteBranchRegEx)).willReturn(aResponse().withStatus(204)));

    client.deleteBranch("cogeco-it", "drupal", "a_branch");

    wm.verify(deleteRequestedFor(urlPathMatching(deleteBranchRegEx)));
  }

  @Test
  public void itThrowsExceptionsOnNon200Responses() {
    wm.stubFor(
      post(urlPathMatching("/repositories/cogeco-it/deploy_firestorerest/pullrequests"))
        .willReturn(aResponse().withStatus(401))
    );

    assertThrows(BitbucketClientException.class, () -> {
      client.openPullRequest(
        "cogeco-it",
        "deploy_firestorerest",
        new OpenPullRequestBody()
      );
    });
  }
}
